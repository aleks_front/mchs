'use strict';

/*
    This file can be used as entry point for webpack!
 */

const lazyload = require('jquery-lazyload');
const affix = require('bootstrap/js/affix.js');

//START components
const modal = require('components/modal/modal');
//END components

(function ($) {
    $(function () {
        // START lazyload
        $(".img-lazy").lazyload({
            effect : "fadeIn"
        });
        // END lazyload
        // START go back 
        $(document).on('click', '.go-back', function(e){
            if (history.length > 2 || document.referrer.length > 0) {
                e.preventDefault();
                historyBackWFallback($(this).attr('href'));
            }
        });
        // END go back
    });
})(jQuery);

// START go back function
function historyBackWFallback(fallbackUrl) {
    fallbackUrl = fallbackUrl || '/';
    var prevPage = window.location.href;

    window.history.go(-1);

    setTimeout(function(){ if (window.location.href == prevPage) window.location.href = fallbackUrl; }, 500);
}
// END go back function


