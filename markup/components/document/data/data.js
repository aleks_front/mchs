var data = {
    document : {},
    documentAbout : [
        {
            type: 'pdf',
            icon: 'icon icon-pdf',
            title: 'Положение о совете',
            meta: 'PDF, 456 Kb'
        },
        {
            type: 'word',
            icon: 'icon icon-word',
            title: 'Устав совета',
            meta: 'DOCX, 1.3 Mb'
        },
        {
            type: 'word',
            icon: 'icon icon-word',
            title: 'Состав участников совета и их должностные обязанности в рамках деятельности совета',
            meta: 'DOCX, 1.3 Mb'
        }
    ]
};