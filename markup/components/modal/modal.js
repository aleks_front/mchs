(function ($) {
    $(function () {

        $(document).on('click', '.getmodal', function(e){
            e.preventDefault();
            var idModal = $(this).attr('href');
            openModal(idModal);
        });

        $(document).on('click', '.modal__close, .modal__overlay', function(e){
            e.preventDefault();
            modalClose();
        });

        $(document).keyup(function (e) {
            if (e.keyCode == 27) {
                modalClose();
            }
        });

        function openModal(idModal){
            if(idModal.length > 0){
                $(idModal).addClass('is-open');
            }
        }

        function modalClose(){
            $('.modal').removeClass('is-open');
            if ($('form.wpcf7-form').length > 0){
                $('form.wpcf7-form')[0].reset();
                $('.wpcf7-not-valid-tip').hide();
                $('.wpcf7-response-output').hide();
            }
        }
        window.modalThankyou = function() {
            modalClose();
            openModal('#thankyou');
            setTimeout(function() {
               modalClose();
            }, 5000);
        };
    });
})(jQuery);
