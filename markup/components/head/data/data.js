head: {
    defaults: {
        title: 'Общественный Совет при ГУ МЧС и Департаменте ГОЧС и ПБ',
        useSocialMetaTags: true
    },
    news: {
        title: 'Новости',
        link: 'news.html',
        useSocialMetaTags: true
    },
    newsDetail: {
        title: 'В районе международного делового центра «Москва-Сити» проведены показные пожарно-тактические учения.',
        link: 'news-detail.html',
        useSocialMetaTags: true
    },
    about: {
        title: 'О совете',
        link: 'about.html',
        useSocialMetaTags: true
    },
    composition: {
        title: 'Состав',
        link: 'members.html',
        useSocialMetaTags: true
    },
    documents: {
        title: 'Документы',
        link: 'documents.html',
        useSocialMetaTags: true
    }
}
